This is the code for the NA62 compilation and running tests.

Use git to download the code to your local area:

```
cd myNA62Dir
git clone ssh://git@gitlab.cern.ch:7999/hcroft/NA62TestCodeFramework.git
``` 

Then edit the `NA62TestCodeFramework/scripts/env.sh` to point to your
code locations.

Use 
```
python ${NA62TESTCODE}/testCode.py -h
```
to see the help information for the package.

To compile, run and check the logs against a reference file for 10 Pi Nu
Nu everts with NA62MC try:
```
cd $NA62MCSOURCE
source ${NA62TESTCODE}/script/env.sh
python ${NA62TESTCODE}/testCode.py -a GeneratePiNuNu
```


