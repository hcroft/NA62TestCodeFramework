############################################################
#
# Compare mean and RMS of histograms in two root files
# output as a text summary and html diff file
#
############################################################

import ROOT
import filecmp,difflib,os

class OutputHistInfo:
    """Class to store info about TH1 and TH2 histograms"""
    def __init__(self, h, path):
        self.name = h.GetName() # every TObject has a name
        self.title = h.GetTitle() #  every TObject has a title
        self.path = path
        self.type = h.IsA().GetName()
        self.entries = None # fill in if function exists
        self.meanX = None # fill in if function exists
        self.rmsX = None # fill in if function exists
        self.meanY = None # fill in if function exists
        self.rmsY = None # fill in if function exists
        # check can access parameters
        if( h.IsA().InheritsFrom("TH1") ):
            self.entries = h.GetEntries()
            self.meanX = h.GetMean(1) # mean of X
            self.rmsX = h.GetRMS(1) # RMS of X
            if( h.IsA().InheritsFrom("TH2") ): # TH2 inherits from TH1
                self.meanY = h.GetMean(2) # mean of Y
                self.rmsY = h.GetRMS(2) # RMS of Y
        if(  h.IsA().InheritsFrom("TTree") ):
            self.entries = h.GetEntries()
    
    def strOut(self):
        string = "name: {}/{}, title: {}, type: {}".\
            format(self.path,self.name,self.title,self.type)
        if( self.entries is not None ):
            string += " entries={}".format(self.entries)
        if( self.meanX is not None ):
            string += ", mean X={}, rms X={}".\
            format(self.meanX,self.rmsX)
        if( self.meanY is not None ):
            string += ",  mean Y={}, rms Y={}".\
            format(self.meanY,self.rmsY)
        string += "\n"
        return string

def makeHistInfo(rf,path=None):
    rf.cd()
    if(path is None):
        path = ""
    else:
        path += "/"
        path += rf.GetName() 
    outputHistInfo = []
    for key in rf.GetListOfKeys():
        if(key.GetClassName() == "TDirectoryFile"):
            subdir = rf.Get(key.GetName())
            outputHistInfo += makeHistInfo(subdir,path) # recurse into subdirectory
        else:
            outputHistInfo.append(OutputHistInfo(rf.Get(key.GetName()),path))
    return outputHistInfo

def stringHistInfo(rf):
    hStrings = []
    for hInfo in makeHistInfo(rf):
        hStrings.append(hInfo.strOut())
    return hStrings

def sumFile(rootFile,summaryFile):
    """Summarise the content of a root file as text"""
    if(not os.path.isfile(rootFile)):
        print "RootConf: Failed to find test file {}".format(newFile)
        return 
    rFile = ROOT.TFile.Open(rootFile)
    summary = stringHistInfo(rFile)
    outFile = open(summaryFile,'w')
    outFile.writelines(summary)
    outFile.close()
    return len(summary)
