#########################################################
#
# Code to customise the default StandardRun.mac file
#
########################################################
import argparse, os, sys

parser = argparse.ArgumentParser(description="Rewrite a mac file to generate specific events")

parser.add_argument("--nEvt", help="Number of events to run", type=int, default=10)
parser.add_argument("--runNumber", help="Set run number", type=int, default=123456789)
parser.add_argument("--randomSeed", help="Set random number seed", type=int, default=123456789)
parser.add_argument("--outputFile", help="Set root output file name", default='pluto.root')
parser.add_argument("--decayType", help="Set forced decay type, see StandardRun.mac for list", type=int, default=0)
parser.add_argument("--inputMac", help="Input mac script to rewrite", default='macros/StandardRun.mac')
parser.add_argument("outputScript", help="Ouput mac file to write")

args = parser.parse_args()
if( not os.path.isfile(args.inputMac) ):
    print 'Failed to find input file {}'.format(args.inputMac)
    sys.exit(1)

outLines = []
for l in open(args.inputMac,'U'):
    if ( len(l) == 1 ) : continue # skip black lines
    if ( l.startswith('#') ) : continue # skip comment lines
    if ( l.startswith('/run/number') ):
        outLines.append('/run/number {}\n'.format(args.runNumber))
    elif (  l.startswith('/random/seed') ):
        outLines.append('/random/seed {}\n'.format(args.randomSeed))
    elif (  l.startswith('/output/fileName') ):
        outLines.append('/output/fileName {}\n'.format(args.outputFile))
    elif (  l.startswith('/decay/type') ):
        outLines.append('/decay/type {}\n'.format(args.decayType))
    elif (  l.startswith('/run/beamOn') ):
        outLines.append('/run/beamOn {}\n'.format(args.nEvt))
    else:
        outLines.append(l)

outF = open(args.outputScript,'w')
outF.writelines(outLines)
outF.close()

        
    
