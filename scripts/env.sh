#!/bin/sh
export NA62ROOT=/na62store/hutchcroft/NA62Current
echo "Setup enviroment for NA62 TestCode scripts"
if [ ! -d $NA62ROOT ]; then
    echo "Default NA62 code location is \"${NA62ROOT}\", which does not exist"
    echo "change this in the script $BASH_SOURCE to match your location"
else
    echo "Current location of repository ssh://git@gitlab.cern.ch:7999/hcroft/NA62TestCodeFramework.git"
    echo "Run: python ${NA62ROOT}/NA62TestCodeFramework/testCode.py --help"
    echo "to see how to use the code"
    export NA62MCSOURCE=${NA62ROOT}/NA62MC
    export NA62RECOSOURCE=${NA62ROOT}/NA62Reconstruction
    export NA62TESTCODE=${NA62ROOT}/NA62TestCodeFramework
    
    if [ -f ${NA62MCSOURCE}/scripts/env.sh ]; then
	source ${NA62MCSOURCE}/scripts/env.sh
    else
	echo "${NA62MCSOURCE}/scripts/env.sh did not exist!"
    fi
    if [ -f ${NA62RECOSOURCE}/scripts/env.sh ]; then
	source ${NA62RECOSOURCE}/scripts/env.sh
    else
	echo "${NA62MCSOURCE}/scripts/env.sh did not exist!"
    fi
    
    export PYTHONDIR=/afs/cern.ch/sw/lcg/releases/Python/2.7.9.p1-df007/x86_64-slc6-gcc48-opt
    export PYTHONPATH=${ROOTSYS}/lib:${NA62TESTCODE}:${PYTHONPATH}
    export PATH=${PYTHONDIR}/bin:${PATH}
fi

