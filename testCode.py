#########################################################
#########################################################
#
# Test NA62 Code: compile and run code, 
# then compare output to reference files
#
#########################################################
#########################################################
import ConfigParser, sys, os, argparse, subprocess
import filecmp,difflib

# Local classes and files
import BuildConf
import RootComp

def findConfig(confDir):
    """ find all testConf files in a directory """
    files = os.listdir(confDir)
    confFiles = []
    for f in files:
        if ( f.endswith('.testConf') ):
            confFiles.append(f)
    return confFiles

def buildConf(configFile):
    """Create the BuildConfig objects from the configFile"""
    cp = ConfigParser.SafeConfigParser()
    cp.read('tests/'+configFile)
    bcList = []
    for section in cp.sections():
        bcList.append(BuildConf.buildConf(section, cp))
    return bcList

def runCommand(command,outname,bc,args,useNcpu=False):
    """Run command in a subprocess and catch the output"""
    commands = "cd {} && ".format(bc.params['rootDir'])
    commands += command
    if( useNcpu ):
        commands += " -j {}".format(args.cpus)
    stdout = "{}.{}.stdout".format(bc.name,outname)
    stderr = "{}.{}.stderr".format(bc.name,outname)
    print
    if( args.dryrun ):
        print "dryrun: would have run command:",commands
    else:
        print "Running command:",commands
    print "stdout to file {} and stderr to {}".format(stdout,stderr)
    d = bc.params["testDir"]
    print "in directory",d
    if(args.dryrun):
        return 0 # do not actually run command
    if(not os.path.isdir(d)):
        try:
            print 'Making output directory {}'.format(d)
            os.makedirs(d)
        except Exception as e:
            print 'Failed to find or make output directory',d
            print 'Exception',e
            sys.exit(1)
    else:
        print 'Using output directory',d
    stdinFile = bc.params["testDir"]+'/'+stdout
    stderrFile = bc.params["testDir"]+'/'+stderr
    status = subprocess.call(commands,stdin=None,
                             stdout=open(stdinFile,'wb'),
                             stderr=open(stderrFile,'wb'),
                             shell=True)
    print "Output status:",status
    if( status != 0 ):
        print 'Failed this step, stopping'
        sys.exit(1)
    print 'File length of stdout',len(open(stdinFile).readlines()),'lines'
    nErr = len(open(stderrFile).readlines())
    if(nErr>0):
        print 'File length of stderr',nErr,'lines, check for warnings and errors (and fix them)!'
    return True

def runTest(bc,args):
    """Run the various components of the test suite"""
    if(args.clean and len(bc.params['cleanCommand'])>0 ):
        runCommand(bc.params['cleanCommand'],'clean',bc,args)
    else:
        print 'Skipping clean step'
    if(args.make and len(bc.params['buildCommand'])>0):
        runCommand(bc.params['buildCommand'],'make',bc,args,True)
    else:
        print 'Skipping make step'
    if(args.execute and len(bc.params['exeFile'])>0):
        if( len(bc.params['preRunScript']) > 0 ):
            runCommand(bc.params['preRunScript'],'preRunScript',bc,args)
        for f in bc.params['requiredFiles'].split():
            if( not os.path.isfile(f) ):
                print 'The required file {} is missing, stopping'.format(f)
                sys.exit(1)
        runCommand(bc.params['exeFile']+" "+bc.params['exeArgs'],
                   'execute',bc,args)
    else:
        print 'Skipping exectue step'
    if(args.outputHistComp):
        if( os.path.isdir(bc.params["testDir"]+'/') ) :
            outList = os.listdir(bc.params["testDir"]+'/')
            print 'outlist',outList
        else:
            print 'Missing output directory'
            outList = []
        for of in outList:
            # skip non root files
            if( not of.endswith('.root') ) : continue
            newFile = "{}/{}".format(bc.params["testDir"],of)
            summFile = newFile+'.summary'
            if(args.dryrun):
                print 'dryrun: Would have summarised {} to {}'.\
                    format(newFile,summFile)
                continue
            nObj = RootComp.sumFile(newFile,summFile)
            print 'Summaried {} into {}\nContained {} objects'.\
                format(newFile,summFile,nObj)
    else:
        print 'Skipping output root file comparison step'
    if(args.outputTextComp):
        if( os.path.isdir(bc.params["testDir"]+'/') ) :
            outList = os.listdir(bc.params["testDir"]+'/')
        else:
            print 'Missing output directory'
            outList = []
        for of in outList:
            # skip root files and existing diff files
            if(of.endswith('.root') or
               of.endswith('.diff.html')) : continue
            newFile = "{}/{}".format(bc.params["testDir"],of)
            refFile = "{}/{}.ref".format(bc.params["referenceDir"],of)
            outFile = "{}/{}.diff.html".format(bc.params["testDir"],of)
            if(args.dryrun):
                print 'dryrun: Would have compared {} and {} diffs to{}'.\
                    format(newFile,refFile,outFile)
                continue 
            if( os.path.isfile(refFile) ):
                newLines=open(newFile,'U').readlines()
                refLines=open(refFile,'U').readlines()
                if(filecmp.cmp(newFile,refFile)):
                    print '{} is the same as the reference'.format(newFile)
                else:
                    diff = difflib.HtmlDiff().make_file(newLines,refLines,
                                                        newFile,refFile,
                                                        context=True,numlines=1)
                    ofDiff = open(outFile,'w')
                    ofDiff.writelines(diff)
                    ofDiff.close()
                    print '{} is not the same as the reference, see {} for the changes (HTML format)'.format(newFile,outFile)
            else:
                print 'Missing {} reference file'.format(refFile)
    else:
        print 'Skipping output text file comparison step'
    return True

def listTests(bcList):
    """Print the list of available tests"""
    print
    print 'List of defined tests:'
    print
    for bc in bcList:
        print bc.name
    print

if __name__ == "__main__":
    epilogtext = """ Use either -a for all, or one or more of the components 
(--clean, --make, --execute, --outputTextComp or --outputHistComp)

 examples of use:

# clean, compile and run NA62MC to generate 10 pi nu nu events
source myNA62dir/NA62TestCodeFramework/scripts/env.sh
cd $NA62MCSOURCE
python ../NA62TestCodeFramework/testCode.py -a GeneratePiNuNu

# test compilation of NA62Reconstruction
source myNA62dir/NA62TestCodeFramework/scripts/env.sh
cd $NA62RECOSOURCE
python ../NA62TestCodeFramework/testCode.py --compile CompileNA62Reconstruction

# test NA62Reconstruction on standard 2014 data file (no compilation)
source myNA62dir/NA62TestCodeFramework/scripts/env.sh
cd $NA62RECOSOURCE
python ../NA62TestCodeFramework/testCode.py --test Reconstruct2014Data

# list available tests
python ../NA62TestCodeFramework/testCode.py -l
"""

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description="Select and run test jobs", 
                                     epilog=epilogtext)
    parser.add_argument("testName", nargs='*', help="Name of test to run")
    parser.add_argument("-l", "--list", help="List names of tests", action="store_true")
    parser.add_argument("-a", "--all", help="Run all components of the test", action="store_true")
    parser.add_argument("-c", "--compile", help="Run clean and make only", action="store_true")
    parser.add_argument("-t", "--test", help="Execute and test output only", action="store_true")
    parser.add_argument("--clean", help="Run clean step", action="store_true")
    parser.add_argument("--make", help="Run make step", action="store_true")
    parser.add_argument("--execute", help="Run exectute step", action="store_true")
    parser.add_argument("--outputTextComp", help="Compare text output to reference", action="store_true")
    parser.add_argument("--outputHistComp", help="Create test summary of root file output", action="store_true")
    parser.add_argument("-n", "--dryrun", help="Print commands test would run only, without running them", action="store_true")
    parser.add_argument("-j", "--cpus", help="Pass -j N to compiler to use N CPUs when compiling", type=int, default=1)
    args = parser.parse_args()

    if(not os.environ.has_key('NA62TESTCODE')):
        print 'Please setup the NA62 testCode enviroment before running this script'
        sys.exit(1)

    configFiles = findConfig(os.getenv("NA62TESTCODE")+"/tests/")
    bcList = []
    for confFile in configFiles:
        bcList += BuildConf.makeBCList(os.getenv("NA62TESTCODE")+"/tests/"+confFile)

    if args.list:
        listTests(bcList)
        sys.exit(0)

    if (not (args.all or args.compile or args.test or
             args.clean or args.make or args.execute or
             args.outputTextComp or args.outputTextComp or args.outputHistComp ) ):
        print "Use either -a for all, -c for compile -t for test (-h for full help)"
        print "Also it is possible to set one or more of the specific components (--clean, --make, --execute, --outputTextComp or --outputHistComp)"
        sys.exit(1)

    if args.all:
        args.clean = True
        args.make = True
        args.execute = True
        args.outputTextComp = True
        args.outputHistComp = True

    if args.compile:
        args.clean = True
        args.make = True
        args.execute = False
        args.outputTextComp = True
        args.outputHistComp = False

    if args.test:
        args.clean = False
        args.make = False
        args.execute = True
        args.outputTextComp = True
        args.outputHistComp = True

    for t in args.testName :
        found = False
        for bc in bcList:
            if t == bc.name :
                found = True
                passed = runTest(bc,args)
                if( not passed ):
                    print 'Test {} failed, stopping'.format(t)
                    sys.exit(1)
        if(not found):
            print 'Test {} was not found'.format(t)
            listTests(bcList)
            sys.exit(1)
