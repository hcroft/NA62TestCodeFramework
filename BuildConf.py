import ConfigParser
import os

class BuildConf:
    """Class that has the information to build and test set of code"""
    def defaultParams(self):
        self.params = { # setup of a test, with some defaults
            'name' : 'GeneratePiNuNu', 
            # command to remove existing compilation
            'cleanCommand' : 'make cleanall', 
            # command to (re)build program
            'buildCommand': 'make all',
            # files required for test to run
            'requiredFiles': '../${NA62TESTCODE}/macros/GeneratePiNuNu.mac',
            # script to run before running the code (e.g. generate mac file)
            'preRunScript': 'python ../${NA62TESTCODE}/makeMac.py --nEvt 10 --outputFile pluto.root --decayType 0 --inputScript macros/StandardRun.mac --outputScript ../${NA62TESTCODE}/macros/GeneratePiNuNu.mac',
            # executable to run program (with path from root dir)
            'exeFile': 'NA62MC',
            # arguments to program (should be a list)
            'exeArgs': "../${NA62TESTCODE}/macros/GeneratePiNuNu.mac" , 
            # directory to run tests in (note deleted and remade each run)
            'testDir': "../${NA62TESTCODE}/outputFiles/GeneratePiNuNu",
             # directory with reference files to compare output to
            'referenceDir': "../${NA62TESTCODE}/refs/GeneratePiNuNu", 
            # base directory of tree (normally ./)
            'rootDir': "./"}

    def __init__(self, name, cp=None):
        """Initialize BuildConf from a parsed config file"""        
        self.name = name
        self.defaultParams() # define parameter set
        if(cp is not None):
            for  var in self.params : # load parameters from config file
                exec 'self.params["{v}"] = cp.get(self.name,"{v}")'.format(v=var)
                if(self.params[var].count("$") != 0):
                    self.params[var] = os.path.expandvars(self.params[var])

    def writeConf(self, confOut):
        """Dump a configured buildConf to confOut.testConfig"""
        cp = ConfigParser.SafeConfigParser()        
        cp.add_section(self.name)
        for  var in self.params : # load parameters from config file
            exec 'cp.set(self.name,"{v}",self.params["{v}"])'.format(v=var)
        # Writing our configuration file 
        with open(confOut+".testConf", 'wb') as configfile:
            cp.write(configfile)                

def makeBCList(confFile):
    cp = ConfigParser.SafeConfigParser()
    cp.read(confFile)
    bcList = []
    for section in cp.sections():
        bcList.append(BuildConf(section,cp))
    return bcList
